package fr.cnam.foad.nfa035.badges.wallet.dao.Impl;

import fr.cnam.foad.nfa035.badges.wallet.dao.BadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.ImageStreamingDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.ImageStreamingSerializer;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.ImageDeserializerBase64StreamingImpl;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.ImageSerializerBase64StreamingImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

public class SingleBadgeWalletDAOImpl implements BadgeWalletDAO {

    private File walletDatabase;

    private static final Logger LOG = LogManager.getLogger(SingleBadgeWalletDAOImpl.class);
    private Object media;


    public SingleBadgeWalletDAOImpl(File wallet){
        walletDatabase = wallet;


    }

    @Override
    public void MultiBadgeWalletDAOImplBadgeWalletDAO(String dbPath) throws IOException {

    }

    /**
     * Permet d'ajouter le badge au Wallet
     *
     * @param image
     * @throws IOException
     */
    public void addBadge(File image) throws IOException{
        ImageStreamingSerializer serializer = new ImageSerializerBase64StreamingImpl();
        serializer.serialize(image, media);
    }


    /**
     * Permet de récupérer le badge du Wallet
     *
     * @param imageStream
     * @throws IOException
     */
    public void getBadge(OutputStream imageStream) throws IOException{
        ImageStreamingDeserializer deserializer = new ImageDeserializerBase64StreamingImpl(imageStream);
        deserializer.deserialize((ImageFrameMedia) media);
    }
}
