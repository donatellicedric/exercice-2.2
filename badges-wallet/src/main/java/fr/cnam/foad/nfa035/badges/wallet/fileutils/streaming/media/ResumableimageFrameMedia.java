package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * A java-doc-er
 */
public interface ResumableimageFrameMedia extends ImageFrameMedia {

    /**
     *
     * @param resume
     * @return
     * @throws IOException
     */
    BufferedReader getEncodedImageReader(boolean resume) throws IOException;


}
