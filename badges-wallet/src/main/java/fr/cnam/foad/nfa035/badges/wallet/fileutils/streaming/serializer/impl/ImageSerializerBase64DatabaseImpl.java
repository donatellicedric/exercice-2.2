package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl.ImageByteArrayFrame;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl.ImageFileFrame;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.AbstractStreamingImageSerializer;
import org.apache.commons.codec.binary.Base64OutputStream;

import java.io.*;
import java.nio.file.Files;

import static com.sun.tools.jdeprscan.CSV.write;

public class ImageSerializerBase64DatabaseImpl
        extends AbstractStreamingImageSerializer<File, ImageFrameMedia> {


    /**
     *
     * @param source
     * @return
     * @throws FileNotFoundException
     */
    public InputStream getSourceInputStream(File source) throws FileNotFoundException { return new FileInputStream(source); }



    @Override
    public OutputStream getSerializingStream(ImageFrameMedia media) throws IOException {
        return new Base64OutputStream(media.getEncodedImageOutput());
    }




    @Override
    public final void serialize(File source, ImageFrameMedia media) throws IOException {
        long numberOfLines = Files.lines(((File)media.getChannel()).toPath()).count();
        long size = Files.size(source.toPath());
        try(OutputStream os = media.getEncodedImageOutput()) {
            Writer writer = ;
            writer.write((long) numberOfLines; (long) size ; );
            writer.flush();
            try(OutputStream eos = getSerializingStream(media)) {
                getSourceInputStream(source).transferTo(eos);
                writer.write("\n");
            }
            writer.flush();
        }
    }


}