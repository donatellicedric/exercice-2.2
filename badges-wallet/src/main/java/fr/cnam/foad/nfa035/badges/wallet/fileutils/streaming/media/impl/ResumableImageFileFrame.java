package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.AbstractImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ResumableimageFrameMedia;

import java.io.*;

/**
 * Implémentation d'ImageFrame pour un simple Fichier comme canal
 */
public class ResumableImageFileFrame extends AbstractImageFrameMedia<File>   {

    private BufferedReader reader;

    public ResumableImageFileFrame(File walletDatabase) {
        super(walletDatabase);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OutputStream getEncodedImageOutput() throws FileNotFoundException {
        return new FileOutputStream(getChannel(), true);
    }

    /**
     *
     * @return
     * @throws IOException
     */
    @Override
    public InputStream getEncodedImageInput() throws IOException {
        return new FileInputStream(getChannel());
    }

    /**
     *
     * @param resume
     * @return
     * @throws IOException
     */
    public BufferedReader getEncodedImageReader(boolean resume) throws IOException {
        if (reader == null || !resume){
            this.reader = new BufferedReader(new InputStreamReader(new FileInputStream(getChannel())));
        }
        return this.reader;
    }

}
