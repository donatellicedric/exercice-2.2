package fr.cnam.foad.nfa035.badges.wallet.dao;

import fr.cnam.foad.nfa035.badges.wallet.dao.Impl.MultiBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl.ImageFileFrame;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.ImageDeserializerBase64StreamingImpl;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.ImageSerializerBase64StreamingImpl;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.ImageStreamingDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.ImageStreamingSerializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

/**
 * DAO simple pour lecture/écriture d'un badge dans un wallet à badge unique
 */
public interface BadgeWalletDAO  {

    /**
     * Constructeur élémentaire
     *
     * @param dbPath
     * @throws IOException
     */
    public void MultiBadgeWalletDAOImplBadgeWalletDAO(String dbPath) throws IOException;

    /**
     * Permet d'ajouter le badge au Wallet
     *
     * @param image
     * @throws IOException
     */
    public void addBadge(File image) throws IOException;

    /**
     * Permet de récupérer le badge du Wallet
     *
     * @param imageStream
     * @throws IOException
     */
    public void getBadge(OutputStream imageStream) throws IOException;

}
