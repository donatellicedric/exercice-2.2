package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl;

import org.apache.commons.codec.binary.Base64InputStream;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ImageDeserializerBase64DatabaseImpl implements DatabaseDeserializer<ResumableImageFrameMedia> {

    private T sourceOutputStream;

    public ImageDeserializerBase64DatabaseImpl ;


    @Override
    public void deserialize(ResumableImageFrameMedia media) throws IOException {
        // 1. Récupération de l'instance de lecture séquentielle du fichier de base csv
        // 2. Lecture de la ligne et parsage des différents champs contenus dans la ligne
        // 3. Désérialisation de l'image Base64 + écriture en clair dans le flux de restitution au format source
    }


    /**
     *
     * @param data
     * @return
     * @throws IOException
     */
    public InputStream getDeserializingStream(String data) throws IOException {
        return new Base64InputStream(new ByteArrayInputStream(data.getBytes()));
    }


    /**
     *
     * @return
     */
    public OutputStream getSourceOutputStream() {
        return sourceOutputStream;
    }

    /**
     * {@inheritDoc}
     * @param os
     * @param <T>
     */

    /**
     *
     * @param os
     * @param <T>
     */
    public <T extends OutputStream> void setSourceOutputStream(T os) {
        this.sourceOutputStream = os;

    }
}